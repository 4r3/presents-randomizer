import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    presents: [
    ],
    counter: 0
  },
  mutations: {
    scramble(state){
      for(let present of state.presents){
        present.target = present.from
      }
      for(let present of state.presents){
        let swappable = state.presents.filter(item=>{
          return !present.doNotTarget.includes(item.target) &&
              !item.doNotTarget.includes(present.target) &&
              present.from !== item.target &&
              item.from !== present.target
        });

        if(swappable.length > 0){
          let pair = swappable[Math.floor(Math.random()*swappable.length)];
          let buffer = present.target
          present.target = pair.target
          pair.target = buffer
        }
      }
    },
    editPresent(state,payload){
      state.presents[payload.id].from = payload.name;
      state.presents[payload.id].doNotTarget = payload.doNotTarget;
    },
    addPresent(state){
      state.presents.push({name:"",doNotTarget:[],target:undefined,uid:state.counter++})
    },
    remove(state,id){
      state.presents.splice(id,1);
    }
  },
  actions:{
    addPresent({commit}){
      commit('addPresent')
    },
    scramble({commit}){
      commit('scramble')
    },
    editPresent({commit},payload){
      commit('editPresent',payload)
    },
    remove({commit},id){
      commit('remove',id)
    }
  },
  getters: {
    presents(state){
      return state.presents
    }
  }
})
